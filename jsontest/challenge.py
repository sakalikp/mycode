import requests
import json

TIMEURL = "http://date.jsontest.com"
IPURL = "http://ip.jsontest.com/"
JSONURL = "http://validate.jsontest.com/"

def main():
    time_res = requests.get(TIMEURL)
    time_json = time_res.json()
    print(time_json)

    ip_res = requests.get(IPURL)
    ip_json = ip_res.json()
    print(ip_json)

    f = open("jsontest/myservers.txt", "r")
    srvs = f.read()
    srvs = srvs.split('\n')
    print(srvs)
    f.close()

    output = {'json': time_json['time'], 'ip': ip_json['ip'], 'mysvrs': srvs}
    output = f"json={json.dumps(output).replace(' ', '')}"

    val_res = requests.get(f"{JSONURL}?{output}")
    val_json = val_res.json()
    print(val_json)

if __name__ == "__main__":
    main()
