#!/usr/bin/env python3
import http.client

def main():

    ## think of this as setting up the connection
    conn = http.client.HTTPConnection("localhost", 9021)
    while True:
        print("Your input ([1]-check conn, [2]-get data):")
        choice = input()
    ## Send an HTTP request and store the HTTP response
    ##    from our webserver
        if choice == '1':
            conn.request('HEAD', '/')

    ## Returns just the response that has been associated with
    ##    the **conn** object.
            res = conn.getresponse()
    
    ## response status and the reason to the screen.
            print(res.status, res.reason)

        if choice == '2':
    ## this time we'll issue GET
            conn.request('GET', '/')
    
    ## res is equal to the response associated with conn
            res = conn.getresponse()
    
    ## print the response status code and reason
            print(res.status, res.reason)
    
    ## page_data is all of the data associated with res 
            page_data = res.read()

    ## this will point out all of the data associated with res
            page_data = page_data.decode("utf-8")
            print(page_data)
            print(type(page_data))
            f = open('http_readin.txt', 'w')
            f.write(page_data)
            f.close()

        else: 
            print("incorrect input")

if __name__ == "__main__":
    main()

