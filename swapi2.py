#!/usr/bin/env python3
"""Alta3 Research
   Star Wars API HTTP response parsing"""

# pprint makes dictionaries a lot more human readable
from pprint import pprint

# requests is used to send HTTP requests (get it?)
import requests

# URL = "https://swapi.dev/luke/force"      # Comment out this line
URL= "https://swapi.dev/api/people/4/"     # Uncomment this line

def main():
    """sending GET request, checking response"""

    # SWAPI response is stored in "resp" object
    resp= requests.get(URL)

    # check to see if the status is anything other than what we want, a 200 OK
    if resp.status_code == 200:
        # convert the JSON content of the response into a python dictionary
        vader= resp.json()
        pprint(vader)
        print(f"{vader['name']} was born in the year {vader['birth_year']}. His eyes are now {vader['eye_color']} and his hair color is {vader['hair_color']}.")
    else:
        print("That is not a valid URL.")

    resp = requests.get(vader["films"][0])
    resp2 = requests.get(vader["starships"][0])

    if resp.status_code == 200 and resp2.status_code == 200:
        film = resp.json()
        sship = resp2.json()

        print(f"He first appeared in the movie {film['title']} and could be found flying around in his {sship['name']}.")


if __name__ == "__main__":
    main()

