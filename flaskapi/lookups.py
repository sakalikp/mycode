import requests

i = 1

while True:
    print("lookup: ", i)
    res = requests.get("http://localhost:2224/fast")
    i += 1
    if res.status_code != 200:
        print("You reached limit!!!")
        print("Status code: ", res.status_code, " (too many requests)")
        break
