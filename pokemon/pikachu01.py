#!/usr/bin/python3

import requests
import pandas as pd
import json
# define base URL
POKEURL = "http://pokeapi.co/api/v2/pokemon/"

def main():

    # Make HTTP GET request using requests, and decode
    # JSON attachment as pythonic data structure
    # Augment the base URL with a limit parameter to 1000 results
    pokemon = requests.get(f"{POKEURL}?limit=10")
    pokemon_text = pokemon.text
    print(type(pokemon_text))
    pokemon_json = pokemon.json()
    print(type(pokemon_json))
    pokemon_df = pd.DataFrame(pokemon_json)
    #pokemon = pd.read_json(json.dumps(pokemon['results']))

    print(pokemon_df.head())

    # Loop through data, and print pokemon names
    #for poke in pokemon["results"]:
        # Display the value associated with 'name'
        #print(poke["name"])
        #print(poke.get("name"))

    #print(f"Total number of Pokemon returned: {len(pokemon['results'])}")

if __name__ == "__main__":
    main()

