#!/usr/bin/python3
from datetime import datetime
import requests
import json
# imports from Django
from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseNotFound

# This view will return a 404 response
def ierror(request):
    return HttpResponseNotFound("Page was not found")
    
def success(request):
    return HttpResponse("Page was found")

def customheader(request):
    x = {}
    x['learning'] = 'Django' # string:string
    x['speed'] = 55          # string:integer
    
    return HttpResponse(headers=x)  # adds our custom headers to the response

def customcode(request):
    return HttpResponse("Working on that", status=201)  # return teh response code 201 "created"

def currenttime(request):

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    return HttpResponse(f"Current time: {current_time}", status=200)

def myip(request):
    res = requests.get(r"https://api.ipify.org?format=json")
    ip = res.json()
    return HttpResponse(f"Your ip: {ip['ip']}", status=200)
